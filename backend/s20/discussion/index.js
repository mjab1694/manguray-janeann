console.log("Hello World");

// Arithmetic Operators
// + - * / %-modulo

	let x = 1397;
	let y = 7831;

	let sum = x + y;
	console.log("Result of addition operator: " + sum);

	// difference

	let difference = x - y;
	console.log("Result of subtraction operator: " + difference);

	// product

	let product = x * y;
	console.log("Result of multiplication operator: " + product);

	// quotient

	let quotient = x / y;
	console.log("Result of division operator: " + quotient);

	// remainder

	let remainder = y % x;
	console.log("Result of Modulo operator: " + remainder);

	let a = 10;
	let b = 5;
	let remainderA = a % b;
	console.log(remainderA);//0

// Assignment Operator
// Basic Assignment Operator (=)

let assingnmentNumber = 8;

// Addition Assignment Operator

assingnmentNumber = assingnmentNumber + 2
assingnmentNumber +=2;
console.log(assingnmentNumber);

// Substraction/Multiplication/Division Assignment Operator

assingnmentNumber -=2;
console.log("Result of -= : " + assingnmentNumber);
assingnmentNumber *=2;
console.log("Result of *= : " + assingnmentNumber);
assingnmentNumber /=2;
console.log("Result of /= : " + assingnmentNumber);

// Multiple Operators and Parentheses

/*

	1. 3 * 4 =12
	2. 12 / 5 = 2.4
	3. 1 + 2 = 3
	4. 3 - 2.4 = 0.6

*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);

/*
	1. 4 / 5 = 0.8
	2. 2 - 3 = 1
	3. -1 * 0.8 = -0.8
	4.  1 + 0.8 = 0.2
 */

let pemdas = 1 + (2-3) * (4/5);
console.log(pemdas)


// Increment and Decrement
// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to

let z = 1;

let increment = ++z

// the value of "z" is added by a value of one before returning the value and storing it in the variable "increment"

console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);

// the value of z is returned and store in the variable "increment" then the valiue of z is increased by one

increment = z++;
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment: " + z);


// pre-decrement
// decrease then reassing
let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrementt: " + z);

// post-decrement
// reassign then decrease
decrement = z--;
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrementt: " + z);

// Type Coercion
/* Type Coercion is the automatic or implicit conversion of  values from one data type to another */

let numA = 10;
let numB = 12;

let coercion = numA + numB;
console.log(coercion);//"1012"
console.log(typeof coercion);

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

// the boolean "true" is associated with the value of 1

let numE = true + 1;
console.log(numE);

// the boolean "false" is associated with the value 0

let numF = false + 1;
console.log(numF);


// Comparison Operators

let juan = 'juan';

// Equality Operator
console.log("===")
console.log(1 === 1);//true
console.log(1 === 2);//false
console.log(1 === '1');//false
console.log(0 === false);//true
console.log('juan' === 'juan');//true
console.log('juan' === juan);//true

// Innequality Operator (!=)
console.log("!==")
console.log(1 != 1);//false
console.log(1 != 2);//true
console.log(1 != '1');//false
console.log(0 != false);//false
console.log('juan' != 'juan');//false
console.log('juan' != juan);//false
