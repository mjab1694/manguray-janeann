console.log("Hello B297!");

//Functions
	//Parameters and Arguments

/*	function printname(){

		let nickname = prompt("Enter your nickname: ");
		console.log("Hi, " + nickname);
	}

	printname();*/

		function printname(name){

		console.log("Hi, " + name);
	}

	printname("Jane");

	let sampleVariable = "Cardo";

	printname(sampleVariable);


	function checkDivisibiltyby4(num){

		let remainder = num % 4;
		console.log("The remainder of" + num + " divided by 4 is: " + remainder);	
		let isDivisibleBy4 = remainder === 0;
		console.log("Is " + num + " divisible by 4?");
		console.log(isDivisibleBy4);

	}

	checkDivisibiltyby4(56);
	checkDivisibiltyby4(95);
	checkDivisibiltyby4(444);

	// Functions as arguments
	// Function parameters can also accept other functions as arguments

	function argumentFunction(){
		console.log("This function was passed as an argument before the message was printed.");
	};

	function invokeFunction(argumentFunction){
		argumentFunction();
	}

	// Adding and removing the parentheses "()" impacts the output of JS heavily
	// When a function is used with parentheses "()", it denotes invoking/calling a function
	// A function used without a parentheses is normally associated with using the function as an argument to another function

	invokeFunction(argumentFunction);

	// Using multiple parameters

	function createFullName(firstName, middleName, lastName){
		console.log(firstName + ' ' + middleName + ' ' + lastName);
	}

	createFullName('Juan', 'Dela', 'Cruz');
	createFullName('Cruz', 'Dela', 'Juan');
	createFullName('Juan', 'Dela');
	createFullName('Juan','','Cruz');
	createFullName('Juan', 'Dela' , 'Cruz', 'III');

	// Using variables as arguments

	let firstName = "John";
	let middleName = "Doe";
	let lastName = "Smith";

	createFullName(firstName,middleName,lastName);

	/*create a function called printFriends

	3 parameters
	friend1, friend2, friend3

	*/

	function printFriends(friend1,friend2,friend3){
		console.log("My three friends are: " + friend1 + ", " + friend2 + ", " + friend3 + ".")
	}

	printFriends("Harlene", "Ric2x", "Carla");

	// Return Statement

	function returnFullName(firstName, middleName,lastName){

		return firstName + ' ' + middleName + ' ' + lastName;
		console.log("This will not be printed!");
	}

	let completeName1 = returnFullName("Monkey", "D", "Luffy");
	let completeName2 = returnFullName("Cardo", "Tanggol", "Dalisay");

	console.log(completeName1 + " is my bestfriend");
	console.log(completeName2 + " is my friend");


	// Mini Activity

	function getSquareArea(side){

		return side**2
	};

	let areaSquare = getSquareArea(4);

	console.log("The result of getting the area of a square with a lenght of 4: ")
	console.(areaSquare);

	function computeSum(num1, num2, num3){

		return num1 + num2 + num3;

	}

	let sumOfThreeDigits = compute(1,2,3);
	console.log("The sum of three numbers are: ");
	console.log(sumOfThreeDigits);//6

	function compareToOneHundred(num){

		return num === 100;
	}

	let booleanValue = compareToOneHundred(99);
	console.log("Is this one hundred?");
	console.log(booleanValue);