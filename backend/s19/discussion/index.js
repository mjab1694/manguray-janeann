//Syntax, Statements, and Comments

// For us to create a comment, we use Ctrl + /

console.log("We tell the computer to log this in the console.");
alert("We tell the computer to display an alert with this message!")

//Statement - in programming are instructions that we tell the computer to perform
//JS Statements usually end with semicolon (;)

//Syntax - it is the set of rules that describes how statements must be constructed

//Comments
//For us to create a comment, we use Ctrl + /
//We have single line and multi-line comments

// console.log("Hello");

/* alert("This is an alert!");
alert("This is another alert");*/

//Whitespaces (spaces and line breaks) can impact functionality in may computer languages, but not in JS.

//Varirables
//This is used to contain data

//Declare Variables
	//tell our devices that a variableName is created and is ready to store data
		//let/const variableName;

		let myvariable;
		console.log(myvariable);//undefined

		//Variables must be declared first before they are used
		//Using variables before they are declared will return an error
		let hello;
		console.log(hello);

		//declaring and initializing variables
		//Syntax
			//let/const variableName = initialvalue;

		let productName = 'desktop computer'
		console.log(productName)

		let productPrice = 18999;
		console.log(productPrice);

		const interest = 3.539;

		//re-assigning variable values

		productName = 'Laptop';
		console.log(productName)

		// we cannot replace constant values
		// interest = 3.5;
		// console.log(interest);

		// let variable cannot be re-declared within its scope
		let friend = 'Kate';
		friend = 'Jane';

		// Declare a variable
		let supplier;

		//Initialization is done after the variable has been declared
		supplier = "John Smith Tradings";

		// This is consided as re-assingment because its initial value was already declared
		supplier = "Zuitt Store";

		// We cannot declare a const variable without initialization.
		/*const pi = 3.1416;
		console.log(pi);*/

		// Multiply variable declarations
		let productCode= 'DC017', productBrand = 'Dell';
		// const productBrand = 'Deli';
		console.log(productCode,productBrand);

		// Data Types

		// Strings

		let country = 'Philippines';
		let provice = "Metro Manila";

		// Concatenating Strings

		let fullAddress = provice + ', ' + country;
		console.log(fullAddress);

		let greeting = 'I live in the ' + country; 
		console.log(greeting);

		// the escape character (/) in strings in combination with other characers can produce different effects

		// "\n" referts to creating a new line in between text

		let mailAddress = "Metro Manila\n\n\n\nPhilippines";
		console.log(mailAddress);

		let message = "John's employees went home early";
		console.log(message);
		message = 'John\'s employees went home early'
		console.log(message);

		// Numbers
		let headcount = 26;
		console.log(headcount);
		let grade = 98.7;
		console.log(grade);
		let planetDistance = 2e10;
		console.log(planetDistance);

		// Combine text and strings
		console.log("John's first grade last quaster is " + grade);

		// Boolean

		let isMarried = false;
		let isGoodConduct = true;
		console.log(isMarried);

		console.log("isGoodConduct: " + isGoodConduct);

		// Arrays


		// Syntax
			// let/const arrayName = [elementtA, elementB, ....]
		let grades = [98.7,92.1,90.7,98.6];
		console.log(grades);

		// Objects

		let myGrades = {
			firstGrading: 98.7,
			secondGrading: 92.1,
			thirdGrading: 90.7,
			fourthGrading: 94.6
		}
		console.log(myGrades);

		let person = {

			fullName: 'Juan Dela Cruz',
			age: 35,
			isMarried: false,
			contact: ["+639123456789", "8700"],
			address:{
				houseNumber: '345',
				city: 'Manila'
			}
		}
		console.log(person);

		// type of operator
		console.log(typeof person)