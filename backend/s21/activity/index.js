/*
    1. Create a function named getUserInfo which is able to return an object. 

        The object returned should have the following properties:
        
        - key - data type

        - name - String
        - age -  Number
        - address - String
        - isMarried - Boolean
        - petName - String

        Note: Property names given is required and should not be changed.

        To check, create a variable to save the value returned by the function.
        Then log the variable in the console.

        Note: This is optional.

*/


function getUserInfo() {
  return {
    name: "Jane Ann Manguray",
    age: 25,
    address: "123 Anywhere City",
    isMarried: false,
    petName: "Tobi"
  };
}

var userInfo = getUserInfo();

console.log(userInfo);



/*
    2. Create a function named getArtistsArray which is able to return an array with at least 5 names of your favorite bands or artists.
        
        - Note: the array returned should have at least 5 elements as strings.
                function name given is required and cannot be changed.


        To check, create a variable to save the value returned by the function.
        Then log the variable in the console.

        Note: This is optional.
    
*/

function getArtistsArray() {
  return ["Fall Out Boy", "The 1975", "Taylor Swift", "Anne Marie", "Kim Petras"];
}

var artistsArray = getArtistsArray();

console.log(artistsArray);





/*
    3. Create a function named getSongsArray which is able to return an array with at least 5 titles of your favorite songs.

        - Note: the array returned should have at least 5 elements as strings.
                function name given is required and cannot be changed.

        To check, create a variable to save the value returned by the function.
        Then log the variable in the console.

        Note: This is optional.
*/

function getSongsArray() {
  return ["Save Rock and Roll", "Robbers", "Enchancted", "Breathing", "Heart to Break"];
}

var songsArray = getSongsArray();

console.log(songsArray);


/*
    4. Create a function named getMoviesArray which is able to return an array with at least 5 titles of your favorite movies.

        - Note: the array returned should have at least 5 elements as strings.
                function name given is required and cannot be changed.

        To check, create a variable to save the value returned by the function.
        Then log the variable in the console.

        Note: This is optional.
*/

function getMoviesArray() {
  return ["Movie 1", "Movie 2", "Movie 3", "Movie 4", "Movie 5"];
}

var moviesArray = getMoviesArray();

console.log(moviesArray);


/*
    5. Create a function named getPrimeNumberArray which is able to return an array with at least 5 prime numbers.

            - Note: the array returned should have numbers only.
                    function name given is required and cannot be changed.

            To check, create a variable to save the value returned by the function.
            Then log the variable in the console.

            Note: This is optional.
            
*/

function getPrimeNumberArray() {
  var primeNumbers = [];
  var number = 2;

  while (primeNumbers.length < 5) {
    if (isPrime(number)) {
      primeNumbers.push(number);
    }
    number++;
  }

  return primeNumbers;
}

function isPrime(num) {
  for (var i = 2, sqrt = Math.sqrt(num); i <= sqrt; i++) {
    if (num % i === 0) {
      return false;
    }
  }
  return num > 1;
}

var primeNumberArray = getPrimeNumberArray();

console.log(primeNumberArray);


//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        getUserInfo: typeof getUserInfo !== 'undefined' ? getUserInfo : null,
        getArtistsArray: typeof getArtistsArray !== 'undefined' ? getArtistsArray : null,
        getSongsArray: typeof getSongsArray !== 'undefined' ? getSongsArray : null,
        getMoviesArray: typeof getMoviesArray !== 'undefined' ? getMoviesArray : null,
        getPrimeNumberArray: typeof getPrimeNumberArray !== 'undefined' ? getPrimeNumberArray : null,

    }
} catch(err){


}